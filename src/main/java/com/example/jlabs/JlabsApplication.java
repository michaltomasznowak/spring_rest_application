package com.example.jlabs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JlabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(JlabsApplication.class, args);
	}

}
